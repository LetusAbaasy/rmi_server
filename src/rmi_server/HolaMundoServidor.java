/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi_server;
import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.net.*;
import java.io.*;
/**
 *
 * @author M4RS
 */
public class HolaMundoServidor {
   public static void main(String args[]){
      InputStreamReader ent = new InputStreamReader(System.in);
      BufferedReader buf = new BufferedReader(ent);
      String numPuerto, URLRegistro;
      try{
         System.out.println("Introducir el numero de puerto del registro de RMI:");
         numPuerto = buf.readLine().trim();
         int numeroPuertoRMI = Integer.parseInt(numPuerto);
         arrancarRegistro(numeroPuertoRMI);
         HolaMundoImpl objExportado = new HolaMundoImpl();
         URLRegistro =  "rmi://localhost:"+numPuerto+"/holaMundo";
         Naming.rebind(numPuerto, objExportado);
         System.out.println("Servidor registrado. El registro contiene actualmente:");
         listaRegistro(URLRegistro);
         System.out.println("Servidor HolaMundo Preparado.");
      }catch (Exception excr){
         System.out.println("Excepcion en: HolaMundoServidor.main: "+excr);
      }
   }
   private static void arrancarRegistro(int numPuertoRMI) throws RemoteException{
      try{
         Registry registro = LocateRegistry.getRegistry(numPuertoRMI);
         registro.list();
      }catch (RemoteException e){
         System.out.println("El Registro RMI no se puede localizar en el puerto: "+numPuertoRMI);
         Registry registro = LocateRegistry.createRegistry(numPuertoRMI);
         System.out.println("Registro RMI creado en el puerto: "+numPuertoRMI);
      }
   }
   
   private static void listaRegistro(String URLRegistro) throws RemoteException, MalformedURLException{
      System.out.println("Registro: "+ URLRegistro +" Contiene:");
      String [] nombres = Naming.list(URLRegistro);
      for (int i = 0; i < nombres.length; i++) {
         System.out.println(nombres[i]);
      }
   }
}
