/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi_server;
import java.rmi.*;
import java.rmi.server.*;
/**
 *
 * @author M4RS
 */
public class HolaMundoImpl extends UnicastRemoteObject 
implements  HolaMundoInt{
     public HolaMundoImpl() throws RemoteException{
        super();
     }
     
   /**
    *
    * @param nombre
    * @return
    * @throws RemoteException
    */
     @Override
   public String decirHola(String nombre) throws RemoteException{
          return "Hola Mundo"+ nombre;
      }
}
